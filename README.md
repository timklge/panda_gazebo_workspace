# A Complete Panda Gazebo Workspace

## Important Remarks:

- Much of the code in the contained submodules is based on the information [Erdal Pekel's Blog](https://erdalpekel.de/). **Thanks, Erdal!**
- The state outside of the master branch must be considered as "work-in-progress". Only the master branch is stable here.
- Currently, only very simple use cases are contained here. We will gradually update the code. Watch this space!
- The examples only work when *all* projects in this workspace are checked out.

## Requirements

- Ubuntu 18.4
- ROS Melodic

## Installation

- There are two ways to install, using git submodules or by cloning the required ros packages individually.
    - **Either** clone this repo and its submodules (recommended for fresh installations):
        - as a guest: `git clone --recurse-submodules https://git-st.inf.tu-dresden.de/ceti/ros/panda_gazebo_workspace.git panda_gazebo_workspace`
        - as a project member with a registered ssh key: `git clone --recurse-submodules git@git-st.inf.tu-dresden.de:ceti/ros/panda_gazebo_workspace.git panda_gazebo_workspace`
        - change into the workspace `cd panda_gazebo_workspace`
    - **Or** clone the provided packages individually (recommended for existing ROS workspaces or if you do not understand or like submodules)
        - if you do not have a catkin workspace yet, create one `mkdir -p panda_gazebo_workspace/src`
        - change into a `src` directory of a catkin workspace, e.g. with `cd panda_gazebo_workspace/src`
        - clone the four provided packages into the `src` directory (use https if you are not registered or do not have configured an ssh key yet)
        - `git clone https://git-st.inf.tu-dresden.de/ceti/ros/franka_description.git` **or** `git clone git@git-st.inf.tu-dresden.de:ceti/ros/franka_description.git`
        - `git clone https://git-st.inf.tu-dresden.de/ceti/ros/panda_moveit_config.git` **or** `git clone git@git-st.inf.tu-dresden.de:ceti/ros/panda_moveit_config.git`
        - `git clone https://git-st.inf.tu-dresden.de/ceti/ros/panda_simulation.git` **or** `git clone git@git-st.inf.tu-dresden.de:ceti/ros/panda_simulation.git`
        - `git clone https://git-st.inf.tu-dresden.de/ceti/ros/sample_applications.git` **or** `git clone git@git-st.inf.tu-dresden.de:ceti/ros/sample_applications.git`
        - change into the workspace main directory `cd ..`
- install ROS package dependencies `rosdep install --from-paths .`
- build the workspace `catkin build`
- source the config: depending on your shell
    - `source devel/setup.bash` (default)
    - `source devel/setup.sh`
    - `source devel/setup.zsh`

## Simulations

See the [README.md of the submodule](../../../../sample_applications/-/blob/master/README.md) for more information.

- For planning and simulation based on rviz: `roslaunch panda_simulation simulation.launch`
- Execution of a simple motion: `roslaunch sample_applications sample_simple_simulation.launch`
- Execution of a simple motion costraint by a blocking object: `roslaunch sample_applications sample_constraint_simulation.launch`
- Execution of a velocity constraint cartesian trajectory: `roslaunch sample_applications simulation.launch`

## Docker

To run via docker on any up-to-date linux system:

- Clone this repo and its submodules (recommended for fresh installations):
    - as a guest: `git clone --recurse-submodules https://git-st.inf.tu-dresden.de/ceti/ros/panda_gazebo_workspace.git panda_gazebo_workspace`
    - as a project member with a registered ssh key: `git clone --recurse-submodules git@git-st.inf.tu-dresden.de:ceti/ros/panda_gazebo_workspace.git panda_gazebo_workspace`
- Run `./docker-run.sh roslaunch sample_applications sample_simple_simulation.launch` to build and run a docker image tagged with `ceti-panda-gazebo-workspace`